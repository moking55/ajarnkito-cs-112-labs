price_list = ["0", "40000", "50000", "50000", "55000"]
studentID = input("Student ID : ")
status = input("Status (D,I) : ")
year = int(input("Year (1,2,3,4) : "))

if (status == "I"):
    print(f"Student ID {studentID} has to pay {int(int(price_list[year]) * 1.5)} baht")
else:
    print(f"Student ID {studentID} has to pay {int(price_list[year])} baht")