m = int(input("Enter number m : "))
n = int(input("Enter number n : "))

if (n > 0):
    if (m % n == 0):
        print(f"{m} is multiple of {n}")
    elif (m % n != 0):
        print(f"{m} is not multiple of {n}")
    else:
        print("Error: 0 is negative or zero")
else:
    print(f"Error: {n} is negative or zero")