a = float(input("Enter number a : "))
b = float(input("Enter number b : "))
operator = input("Select an operator (+, -, *, /) : ")

if(operator == "+"):
    print(f"{a} {operator} {b} = {a+b}")
elif(operator == "-"):
    print(f"{a} {operator} {b} = {a-b}")
elif(operator == "*"):
    print(f"{a} {operator} {b} = {a*b}")
elif(operator == "/"):
    print(f"{a} {operator} {b} = {a/b}")
else:
    print("Error: invalid operator")