grade = input("Enter your grade : ").lower()

if (grade == "a"):
    print("Your score is 4.0")
elif (grade == "b"):
    print("Your score is 3.0")
elif (grade == "c"):
    print("Your score is 2.0")
elif (grade == "d"):
    print("Your score is 1.0")
elif (grade == "f"):
    print("Your score is 0.0")
else:
    print("Your grade is incorrect")