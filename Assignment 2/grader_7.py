def quadrant(x, y):
    if (x > 0 and y > 0):
        print(f"({x},{y}) = Q1")

    elif (x < 0 and y > 0):
        print(f"({x},{y}) = Q2")

    elif (x < 0 and y < 0):
        print(f"({x},{y}) = Q3")

    elif (x > 0 and y < 0):
        print(f"({x},{y}) = Q4")

    elif (x == 0 and y > 0):
        print (f"({x},{y}) = y-intercept")

    elif (x == 0 and y < 0):
        print (f"({x},{y}) = y-intercept")

    elif (y == 0 and x < 0):
        print(f"({x},{y}) = x-intercept")

    elif (y == 0 and x > 0):
        print(f"({x},{y}) = x-intercept")

    else:
        print(f"({x},{y}) = Origin")


# Driver code
x = int(input("Enter coordinate x : "))
y = int(input("Enter coordinate y : "))
quadrant(x, y)