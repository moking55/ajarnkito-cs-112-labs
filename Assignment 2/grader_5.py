def isPositive(num, isEven):
    if (num > 0):
        print(num, "is positive", isEven)
    else:
        print(num, "is negative", isEven)

n = int(input("Enter number : "))
if (n == 0):
    print(0, "is zero number")
elif (n % 2):
    isPositive(n, "odd number")
else:
    isPositive(n, "even number")